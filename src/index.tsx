import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Hello from './containers/Hello';
// import App from './App';
import './index.css';

import { createStore } from 'redux';
import registerServiceWorker from './registerServiceWorker';

import { enthusiasm } from './reducers/index';
import { IStoreState } from './types/index';

import { Provider } from 'react-redux';

const store = createStore<IStoreState, any, any, any>(enthusiasm, {
    enthusiasmLevel: 1,
    languageName: 'Typescript'
});

ReactDOM.render(
    <Provider store={store}>
        <Hello />
    </Provider>,
    document.getElementById('root') as HTMLElement
);
registerServiceWorker();
